﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;


namespace IT_1
{
    public partial class Form1 : Form
    {
        UInt16 len_s;
        double A_1, A_2, A_3, A_, D1, D2, D3, D;
        UInt16  X1, X2, X3;
        private Data FormStore;
        private Recover MHJ;

        private Double[] InSignal_data;
        private Double[] OutSignal_data;
        private bool Timer;

       

       
        public Form1()
        {
            InitializeComponent();
            FormStore = new Data();
            MHJ = new Recover();
            Timer= true;
            Timer_data.Tick += new EventHandler(TimerEventProcessor);
            Timer_data.Interval = 1;
        }
        private void SendChart(Chart ChartI, Double[] Data)
        {
            ChartI.Series.Clear();

            Double Maxval = 0.0;
            Series NewSeries = new Series("");
            for (UInt16 i = 0; i < Data.Length; i++)
            {
                NewSeries.Points.AddXY(i, Data[i]);

                if (Data[i] > Maxval)
                {
                    Maxval = Data[i];
                }
            }
            NewSeries.ChartType = SeriesChartType.Line;

            ChartI.Series.Add(NewSeries);

            ChartI.ChartAreas[0].AxisX.Minimum = 0;
            ChartI.ChartAreas[0].AxisX.Maximum = Data.Length;
            ChartI.ChartAreas[0].AxisY.Maximum = Maxval * 1.05;

            ChartI.Invalidate();
        }

          private void SecondaryChart(Chart ChartI, Double[] Data)
        {
            Double Maxval = ChartI.ChartAreas[0].AxisY.Maximum;

            Series NewSeries = new Series("");
            for (UInt16 i = 0; i < Data.Length; i++)
            {
                NewSeries.Points.AddXY(i, Data[i]);

                if (Data[i] > Maxval)
                {
                    Maxval = Data[i];
                }
            }
            NewSeries.ChartType = SeriesChartType.Line;

            ChartI.Series.Add(NewSeries);
            ChartI.ChartAreas[0].AxisY.Maximum = Maxval * 1.05;
            ChartI.Invalidate();
        }

        private void build_signal_Click(object sender, EventArgs e)
        {
            //вычисление  и отрисовка сигнала
            len_s = UInt16.Parse(len.Text);
            A_1 = Double.Parse(A1.Text); A_2 = Double.Parse(A2.Text); A_3 = Double.Parse(A3.Text);
            X1 = UInt16.Parse(x_1.Text); X2 = UInt16.Parse(x_2.Text); X3 = UInt16.Parse(x_3.Text);
            D1 = Double.Parse(d1.Text); D2 = Double.Parse(d2.Text); D3 = Double.Parse(d3.Text);
            Double[] Signal_data = FormStore.GenSignal(len_s, A_1, A_2, A_3, D1, D2, D3, X1, X2, X3);
            SendChart(ChartSignal, Signal_data);

            //вычисление и отрисовка импульсной характеристики

            Double D = Double.Parse(d.Text); Double A_ = Double.Parse(A.Text);
            Double[] Im_h_data = Data.Gen_Im_h(len_s, A_, D);


            //вычисление и отриcовка выходного сигнала

            Double[] Y_data = Data.Convol(Im_h_data, Signal_data);


            if (shum.Checked)
            {
                Double NoiseLevel = Double.Parse(proc_energy.Text);
                Y_data = Data.NoiseGenNormAdd(Signal_data, NoiseLevel);
            }

            SendChart(chart_v_S, Y_data);
            SendChart(Chart_im_h, Im_h_data);
        }

        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            Timer_data.Stop();

            Double[] Recovery_data = new Double[InSignal_data.Length];
            Timer = MHJ.Iterate(ref Recovery_data);

            // Вывод квадратичных отклонений сигналов:
            Double Func = 0.0;
            len_s = UInt16.Parse(len.Text);
         
            Double D = Double.Parse(d.Text); Double A_ = Double.Parse(A.Text);
            Double[] Im_h_data = Data.Gen_Im_h(len_s, A_, D);


            for (UInt16 i = 0; i < len_s; i++)
            {
               Func += (InSignal_data[i] - Recovery_data[i]) * (InSignal_data[i] - Recovery_data[i]);
            }
           Func /= (double)(len_s);

            Double Func_out = 0.0;
            Double[] OutRecovery_data = Data.Convol(Recovery_data, Im_h_data);

            for (UInt16 i = 0; i < OutSignal_data.Length; i++)
            {
                Func_out += (OutSignal_data[i] - OutRecovery_data[i]) * (OutSignal_data[i] - OutRecovery_data[i]);
            }
            Func_out /= (double)(OutSignal_data.Length);

           
            func.Text = String.Format("{0:E}", Func_out);

            SendChart(ChartSignal, InSignal_data);
            SecondaryChart(ChartSignal, Recovery_data);
            SendChart(chart_v_S, OutSignal_data);
            SecondaryChart(chart_v_S, OutRecovery_data);

            if (Timer)
            {
                build_signal.Enabled = true;
                vosst.Enabled = true;
                BnPause.Checked = false;
                BnPause.Enabled = false;
                BnStop.Enabled = false;

                MessageBox.Show(
                    "The process of recovery have finished working after " + MHJ.GetCalc() + " iterations.",
                    "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (!(BnPause.Checked))
                {
                    Timer_data.Start();
                }                     
            }
        }
        private void vosst_Click(object sender, EventArgs e)
        {
            // Подготовка сигналов:
            len_s = UInt16.Parse(len.Text);
            A_1 = Double.Parse(A1.Text); A_2 = Double.Parse(A2.Text); A_3 = Double.Parse(A3.Text);
            X1 = UInt16.Parse(x_1.Text); X2 = UInt16.Parse(x_2.Text); X3 = UInt16.Parse(x_3.Text);
            D1 = Double.Parse(d1.Text); D2 = Double.Parse(d2.Text); D3 = Double.Parse(d3.Text);
            Double D = Double.Parse(d.Text); Double A_ = Double.Parse(A.Text);
            UInt16 Prec = UInt16.Parse(t_vost.Text);

            Double[] Im_h_data = Data.Gen_Im_h(len_s, A_, D);

            InSignal_data = FormStore.GenSignal(len_s, A_1, A_2, A_3, D1, D2, D3, X1, X2, X3);
            OutSignal_data = Data.Convol(InSignal_data, Im_h_data);
            if (shum.Checked)
            {
                Double NoiseLevel = Double.Parse(proc_energy.Text);
                OutSignal_data = Data.NoiseGenNormAdd(OutSignal_data, NoiseLevel);
            }

            Double[] Recovery_data = MHJ.InitRec(Im_h_data, OutSignal_data, Prec);
            SendChart(ChartSignal, InSignal_data);
            SecondaryChart(ChartSignal, Recovery_data); ;

            // Запуск таймера:
            Timer = false;
            Timer_data.Start();

            build_signal.Enabled = false;
            vosst.Enabled = false;
            BnPause.Checked = false;
            BnPause.Enabled = true;
            BnStop.Enabled = true;



        }

        private void shum_CheckedChanged(object sender, EventArgs e)
        {
            proc_energy.ReadOnly = !(shum.Checked);
        }
        private void BnStop_Click(object sender, EventArgs e)
        {
            Timer_data.Stop();
            Timer = true;
            build_signal.Enabled = true;
            vosst.Enabled = true;
            BnPause.Checked = false;
            BnPause.Enabled = false;
            BnStop.Enabled = false;

            MessageBox.Show(
                "The process of recovery have been cancelled after " + MHJ.GetCalc() + " iterations.",
                "Abort", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void BnPause_CheckedChanged(object sender, EventArgs e)
        {
            if (BnPause.Checked)
            {
                Timer_data.Stop();
            }
            else if (!Timer)
            {
                Timer_data.Start();
            }
        }
       
        
    }
}
