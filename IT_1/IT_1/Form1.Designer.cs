﻿namespace IT_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.build_signal = new System.Windows.Forms.Button();
            this.vosst = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.x_3 = new System.Windows.Forms.TextBox();
            this.x_2 = new System.Windows.Forms.TextBox();
            this.x_1 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.len = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.d1 = new System.Windows.Forms.TextBox();
            this.A3 = new System.Windows.Forms.TextBox();
            this.d2 = new System.Windows.Forms.TextBox();
            this.A2 = new System.Windows.Forms.TextBox();
            this.d3 = new System.Windows.Forms.TextBox();
            this.A1 = new System.Windows.Forms.TextBox();
            this.Timer_data = new System.Windows.Forms.Timer(this.components);
            this.A = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.d = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.t_vost = new System.Windows.Forms.TextBox();
            this.proc_energy = new System.Windows.Forms.TextBox();
            this.func = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.shum = new System.Windows.Forms.CheckBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.ChartSignal = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart_im_h = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart_v_S = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.BnPause = new System.Windows.Forms.CheckBox();
            this.BnStop = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartSignal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart_im_h)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_v_S)).BeginInit();
            this.SuspendLayout();
            // 
            // build_signal
            // 
            this.build_signal.Location = new System.Drawing.Point(807, 37);
            this.build_signal.Name = "build_signal";
            this.build_signal.Size = new System.Drawing.Size(128, 51);
            this.build_signal.TabIndex = 0;
            this.build_signal.Text = "Построить графики";
            this.build_signal.UseVisualStyleBackColor = true;
            this.build_signal.Click += new System.EventHandler(this.build_signal_Click);
            // 
            // vosst
            // 
            this.vosst.Location = new System.Drawing.Point(807, 94);
            this.vosst.Name = "vosst";
            this.vosst.Size = new System.Drawing.Size(128, 51);
            this.vosst.TabIndex = 3;
            this.vosst.Text = "Восстановить";
            this.vosst.UseVisualStyleBackColor = true;
            this.vosst.Click += new System.EventHandler(this.vosst_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(401, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(197, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Импульсная характеристика";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Входной сигнал";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 260);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Выходной сигнал";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.x_3);
            this.panel1.Controls.Add(this.x_2);
            this.panel1.Controls.Add(this.x_1);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.len);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.d1);
            this.panel1.Controls.Add(this.A3);
            this.panel1.Controls.Add(this.d2);
            this.panel1.Controls.Add(this.A2);
            this.panel1.Controls.Add(this.d3);
            this.panel1.Controls.Add(this.A1);
            this.panel1.Location = new System.Drawing.Point(387, 280);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(470, 235);
            this.panel1.TabIndex = 10;
            // 
            // x_3
            // 
            this.x_3.Location = new System.Drawing.Point(312, 113);
            this.x_3.Name = "x_3";
            this.x_3.Size = new System.Drawing.Size(100, 22);
            this.x_3.TabIndex = 24;
            this.x_3.Text = "35";
            // 
            // x_2
            // 
            this.x_2.Location = new System.Drawing.Point(161, 113);
            this.x_2.Name = "x_2";
            this.x_2.Size = new System.Drawing.Size(100, 22);
            this.x_2.TabIndex = 23;
            this.x_2.Text = "21";
            // 
            // x_1
            // 
            this.x_1.Location = new System.Drawing.Point(11, 113);
            this.x_1.Name = "x_1";
            this.x_1.Size = new System.Drawing.Size(100, 22);
            this.x_1.TabIndex = 22;
            this.x_1.Text = "5";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 200);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(107, 17);
            this.label23.TabIndex = 21;
            this.label23.Text = "Длина сигнала";
            // 
            // len
            // 
            this.len.Location = new System.Drawing.Point(161, 200);
            this.len.Name = "len";
            this.len.Size = new System.Drawing.Size(100, 22);
            this.len.TabIndex = 20;
            this.len.Text = "50";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(158, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 17);
            this.label15.TabIndex = 19;
            this.label15.Text = "Купол 2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 17);
            this.label14.TabIndex = 18;
            this.label14.Text = "Купол 1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(316, 89);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(152, 17);
            this.label13.TabIndex = 17;
            this.label13.Text = "Распожение центра 3";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(312, 143);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 17);
            this.label12.TabIndex = 16;
            this.label12.Text = "Ширина купола 3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(158, 143);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 17);
            this.label11.TabIndex = 15;
            this.label11.Text = "Ширина купола 2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 140);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 17);
            this.label10.TabIndex = 14;
            this.label10.Text = "Ширина купола 1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(158, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(152, 17);
            this.label9.TabIndex = 13;
            this.label9.Text = "Распожение центра 2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(148, 17);
            this.label8.TabIndex = 12;
            this.label8.Text = "Распожение центра1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(173, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Амплитуда2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(312, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "Амплитуда3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Амплитуда1";
            // 
            // d1
            // 
            this.d1.Location = new System.Drawing.Point(11, 160);
            this.d1.Name = "d1";
            this.d1.Size = new System.Drawing.Size(100, 22);
            this.d1.TabIndex = 9;
            this.d1.Text = "4";
            // 
            // A3
            // 
            this.A3.Location = new System.Drawing.Point(312, 62);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(100, 22);
            this.A3.TabIndex = 8;
            this.A3.Text = "2";
            // 
            // d2
            // 
            this.d2.Location = new System.Drawing.Point(161, 163);
            this.d2.Name = "d2";
            this.d2.Size = new System.Drawing.Size(100, 22);
            this.d2.TabIndex = 6;
            this.d2.Text = "7";
            // 
            // A2
            // 
            this.A2.Location = new System.Drawing.Point(161, 62);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(100, 22);
            this.A2.TabIndex = 4;
            this.A2.Text = "4";
            // 
            // d3
            // 
            this.d3.Location = new System.Drawing.Point(315, 163);
            this.d3.Name = "d3";
            this.d3.Size = new System.Drawing.Size(100, 22);
            this.d3.TabIndex = 3;
            this.d3.Text = "4";
            // 
            // A1
            // 
            this.A1.Location = new System.Drawing.Point(11, 62);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(100, 22);
            this.A1.TabIndex = 5;
            this.A1.Text = "5";
            // 
            // A
            // 
            this.A.Location = new System.Drawing.Point(35, 63);
            this.A.Name = "A";
            this.A.Size = new System.Drawing.Size(100, 22);
            this.A.TabIndex = 2;
            this.A.Text = "1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(509, 260);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 17);
            this.label4.TabIndex = 11;
            this.label4.Text = "Данные сигнала";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(696, 280);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 17);
            this.label16.TabIndex = 19;
            this.label16.Text = "Купол 3";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.d);
            this.panel2.Controls.Add(this.A);
            this.panel2.Location = new System.Drawing.Point(898, 285);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(187, 152);
            this.panel2.TabIndex = 20;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(36, 88);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(113, 17);
            this.label20.TabIndex = 21;
            this.label20.Text = "Ширина купола ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(36, 43);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 17);
            this.label18.TabIndex = 11;
            this.label18.Text = "Амплитуда";
            // 
            // d
            // 
            this.d.Location = new System.Drawing.Point(35, 108);
            this.d.Name = "d";
            this.d.Size = new System.Drawing.Size(100, 22);
            this.d.TabIndex = 3;
            this.d.Text = "10";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(848, 455);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(244, 17);
            this.label19.TabIndex = 20;
            this.label19.Text = "Точность восстановления(степень)";
            // 
            // t_vost
            // 
            this.t_vost.Location = new System.Drawing.Point(933, 475);
            this.t_vost.Name = "t_vost";
            this.t_vost.Size = new System.Drawing.Size(100, 22);
            this.t_vost.TabIndex = 5;
            this.t_vost.Text = "3";
            // 
            // proc_energy
            // 
            this.proc_energy.Location = new System.Drawing.Point(941, 198);
            this.proc_energy.Name = "proc_energy";
            this.proc_energy.Size = new System.Drawing.Size(100, 22);
            this.proc_energy.TabIndex = 6;
            this.proc_energy.Text = "10";
            // 
            // func
            // 
            this.func.Location = new System.Drawing.Point(941, 226);
            this.func.Name = "func";
            this.func.Size = new System.Drawing.Size(100, 22);
            this.func.TabIndex = 21;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(895, 265);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(197, 17);
            this.label17.TabIndex = 22;
            this.label17.Text = "Импульсная характеристика";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(775, 198);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(160, 17);
            this.label21.TabIndex = 23;
            this.label21.Text = "Процент энергии шума";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(836, 231);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(99, 17);
            this.label22.TabIndex = 24;
            this.label22.Text = "Функционал=";
            // 
            // shum
            // 
            this.shum.AutoSize = true;
            this.shum.Location = new System.Drawing.Point(839, 162);
            this.shum.Name = "shum";
            this.shum.Size = new System.Drawing.Size(57, 21);
            this.shum.TabIndex = 25;
            this.shum.Text = "Шум";
            this.shum.UseVisualStyleBackColor = true;
            this.shum.CheckedChanged += new System.EventHandler(this.shum_CheckedChanged);
            // 
            // ChartSignal
            // 
            this.ChartSignal.BorderlineColor = System.Drawing.Color.Transparent;
            chartArea10.Name = "ChartArea1";
            this.ChartSignal.ChartAreas.Add(chartArea10);
            this.ChartSignal.Location = new System.Drawing.Point(14, 23);
            this.ChartSignal.Margin = new System.Windows.Forms.Padding(5);
            this.ChartSignal.Name = "ChartSignal";
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series10.Name = "Series1";
            this.ChartSignal.Series.Add(series10);
            this.ChartSignal.Size = new System.Drawing.Size(318, 232);
            this.ChartSignal.TabIndex = 29;
            // 
            // Chart_im_h
            // 
            this.Chart_im_h.BorderlineColor = System.Drawing.Color.Transparent;
            chartArea11.Name = "ChartArea1";
            this.Chart_im_h.ChartAreas.Add(chartArea11);
            this.Chart_im_h.Location = new System.Drawing.Point(441, 12);
            this.Chart_im_h.Margin = new System.Windows.Forms.Padding(5);
            this.Chart_im_h.Name = "Chart_im_h";
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series11.Name = "Series1";
            this.Chart_im_h.Series.Add(series11);
            this.Chart_im_h.Size = new System.Drawing.Size(317, 233);
            this.Chart_im_h.TabIndex = 30;
            // 
            // chart_v_S
            // 
            this.chart_v_S.BorderlineColor = System.Drawing.Color.Transparent;
            chartArea12.Name = "ChartArea1";
            this.chart_v_S.ChartAreas.Add(chartArea12);
            this.chart_v_S.Location = new System.Drawing.Point(15, 285);
            this.chart_v_S.Margin = new System.Windows.Forms.Padding(5);
            this.chart_v_S.Name = "chart_v_S";
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series12.Name = "Series1";
            this.chart_v_S.Series.Add(series12);
            this.chart_v_S.Size = new System.Drawing.Size(317, 233);
            this.chart_v_S.TabIndex = 31;
            // 
            // BnPause
            // 
            this.BnPause.Appearance = System.Windows.Forms.Appearance.Button;
            this.BnPause.Enabled = false;
            this.BnPause.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BnPause.Location = new System.Drawing.Point(957, 37);
            this.BnPause.Margin = new System.Windows.Forms.Padding(5);
            this.BnPause.Name = "BnPause";
            this.BnPause.Size = new System.Drawing.Size(127, 49);
            this.BnPause.TabIndex = 34;
            this.BnPause.Text = "Пауза";
            this.BnPause.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.BnPause.UseVisualStyleBackColor = true;
            this.BnPause.CheckedChanged += new System.EventHandler(this.BnPause_CheckedChanged);
            // 
            // BnStop
            // 
            this.BnStop.Enabled = false;
            this.BnStop.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BnStop.Location = new System.Drawing.Point(957, 96);
            this.BnStop.Margin = new System.Windows.Forms.Padding(5);
            this.BnStop.Name = "BnStop";
            this.BnStop.Size = new System.Drawing.Size(128, 49);
            this.BnStop.TabIndex = 35;
            this.BnStop.Text = "Стоп";
            this.BnStop.UseVisualStyleBackColor = true;
            this.BnStop.Click += new System.EventHandler(this.BnStop_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1098, 643);
            this.Controls.Add(this.BnStop);
            this.Controls.Add(this.BnPause);
            this.Controls.Add(this.chart_v_S);
            this.Controls.Add(this.Chart_im_h);
            this.Controls.Add(this.ChartSignal);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.t_vost);
            this.Controls.Add(this.shum);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.func);
            this.Controls.Add(this.proc_energy);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.vosst);
            this.Controls.Add(this.build_signal);
            this.Name = "Form1";
            this.Text = "М";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChartSignal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart_im_h)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_v_S)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button build_signal;
        private System.Windows.Forms.Button vosst;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox d1;
        private System.Windows.Forms.TextBox A3;
        private System.Windows.Forms.TextBox d2;
        private System.Windows.Forms.TextBox A2;
        private System.Windows.Forms.TextBox d3;
        private System.Windows.Forms.TextBox A1;
        private System.Windows.Forms.Timer Timer_data;
        private System.Windows.Forms.TextBox A;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox t_vost;
        private System.Windows.Forms.TextBox d;
        private System.Windows.Forms.TextBox proc_energy;
        private System.Windows.Forms.TextBox func;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox shum;
        private System.Windows.Forms.TextBox len;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.DataVisualization.Charting.Chart ChartSignal;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart_im_h;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_v_S;
        private System.Windows.Forms.TextBox x_3;
        private System.Windows.Forms.TextBox x_2;
        private System.Windows.Forms.TextBox x_1;
        private System.Windows.Forms.CheckBox BnPause;
        private System.Windows.Forms.Button BnStop;
    }
}

